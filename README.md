# Female Heads of Government

We collect data about the 2020 [SARS-CoV-2
pandemic][(https://en.wikipedia.org/wiki/COVID-19_pandemic) from the
[Johns Hopkins Coronavirus Resource
Center](https://coronavirus.jhu.edu/) and link it to data about the
heads of governments from
[Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page).

# Wikidata

| Item/Property           | Code     |
| ----------------------- | -------- |
| sovereign state         | Q3624078 |
| head of government      | P6       |
| image                   | P18      |
| sex or gender           | P21      |
| is instance of          | P31      |
| ISO 3166-1 alpha-2 code | P297     |

## SPARQL query

```
SELECT ?country ?countryLabel ?code ?head ?headLabel ?gender ?genderLabel ?image
WHERE
{
  ?country   wdt:P31       wd:Q3624078 ;
             wdt:P297      ?code ;
             wdt:P6        ?head .
  ?head      wdt:P21       ?gender ;
             wdt:P18        ?image .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
