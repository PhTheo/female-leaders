#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" correlation between death toll and gender of head of government for SARS-CoV-2 infections """

from COVID19Py import COVID19

from qwikidata.sparql import return_sparql_query_results
from qwikidata.utils import dump_entities_to_json

import pandas as pd

import logging
logging.basicConfig(level=logging.DEBUG)

## JHU CSSE
""" c19_data is a list of dictionaries:nnn
   {'id': 225, 
    'country': 'US', 
    'country_code': 'US', 
    'country_population': 327167434, 
    'province': '', 
    'last_updated': '2020-07-22T09:01:47.621624Z', 
    'coordinates': {'latitude': '40.0', 'longitude': '-100.0'}, 
    'latest': {'confirmed': 3899211, 'deaths': 141995, 'recovered': 0}}
"""
covid19 = COVID19()
jhu_data = covid19.getLocations(rank_by='confirmed')

country_code = [c['country_code'] for c in jhu_data]
population = pd.Series(data=[c['country_population'] for c in jhu_data], index=country_code)
confirmed = pd.Series(data=[c['latest']['confirmed'] for c in jhu_data], index=country_code)
deaths = pd.Series(data=[c['latest']['deaths'] for c in jhu_data], index=country_code)

c19_frame = pd.DataFrame.from_records( 
    data = [c['country_code'], c['country_population'], 
            c['latest']['confirmed'], c['latest']['deaths'],
            for c in jhu_data],
    columns=("country", "population", "confirmed", "deaths"), 
    index="country")
    
logging.info(f"Covid19 data: {len(c19_data)} items loaded, data source: {covid19.data_source}")

## Wikidata
logging.debug("fetching data from WikiData ...")

WD_SOVEREIGN_STATE         = "Q3624078"
WD_HEAD_OF_GOVERNMENT      = "P6"
WD_IMAGE                   = "P18"
WD_SEX_OR_GENDER           = "P21"
WD_IS_INSTANCE_OF          = "P31"
WD_ISO_3166_1_ALPHA_2_CODE = "P297"

sparql_query = f"""
SELECT ?countryLabel ?code ?headLabel ?genderLabel ?imageURL
WHERE
{{
  ?country   wdt:{WD_IS_INSTANCE_OF}           wd:{WD_SOVEREIGN_STATE} ;
             wdt:{WD_ISO_3166_1_ALPHA_2_CODE}  ?code ;
             wdt:{WD_HEAD_OF_GOVERNMENT}       ?head .
  ?head      wdt:{WD_SEX_OR_GENDER}            ?gender ;
             wdt:{WD_IMAGE}                    ?imageURL .
  SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }}
}}
"""

wd = return_sparql_query_results(sparql_query)

wikidata = {}
for item in wd['results']['bindings']:
    def get_key(key):
        l = "Label"
        return key[:-len(l)] if l in key else key
    def get_value(key):
        return item[key]['value']
    code = get_value('code')
    wikidata[code] = { get_key(key): get_value(key)
                       for key in ['countryLabel', 'headLabel', 'genderLabel', 'imageURL'] }

def wd_series(key):
    countries = [ c for c in country_code if c in wikidata.keys() ]
    series = pd.Series(data=[ wikidata[c][key] for c in countries ],
                       index=countries,
                       name=key)
    return series

head = wd_series('head')    
country = wd_series('country')
gender = wd_series('gender')
image = wd_series('imageURL')    

logging.info(f"Wikidata: {len(wikidata.keys())} items found with {len(wd['head']['vars'])} attributes each")

## coerce data into dataframe
df = pd.concat([population, confirmed, deaths, head, country, gender, image], 
               axis=1)
    
